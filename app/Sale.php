<?php

namespace App;

class Sale
{

    public $id;
    private $store; // Add Diagram
    public $date;
    public $itemList = [];
    public $subTotal;
    public $fee;
    public $invoice; // Add Diagram

    function __construct(Store $store)
    {
        $this->store = $store;
    }

    /*
     * Add Diagram
     */
    public static function make()
    {
        return new Sale();
    }

    public function addItem($product)
    {
        array_push($this->itemList, $product);
    }

    public function removeItem($product)
    {
        array_pop($this->itemList, $product);
    }

    public function makeInvoice()
    {
        $this->calculateSubTotal();
        $this->setFee();
        $this->invoice = Invoice::make($this->subTotal, $this->fee,  $this->total(), $this->itemList);

        return $this->invoice;
    }

    private function calculateSubTotal()
    {
        foreach ($this->itemList as $item){
            $this->subTotal +=  $item->price;
        }

        return $this->subTotal;
    }

    protected function setFee()
    {
        return $this->subTotal * 0.19;
    }

    private function total()
    {
        return $this->subTotal + $this->fee;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param mixed $subTotal
     */
    public function setSubTotal($subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return mixed
     */
    public function getItemList()
    {
        return $this->itemList;
    }

    /**
     * @param mixed $itemList
     */
    public function setItemList($itemList)
    {
        $this->itemList = $itemList;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }
}