<?php

namespace App;

class Cashier
{

    public $id;
    public $idTerminal;
    public $name;
    public $sale = Sale::class;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getIdTerminal()
    {
        return $this->idTerminal;
    }

    /**
     * @param mixed $idTerminal
     */
    public function setIdTerminal($idTerminal)
    {
        $this->idTerminal = $idTerminal;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    public function newSale()
    {
        $this->sale = Sale::make();
    }

    public function addProduct($id)
    {
        $this->sale->addItem($id);
    }

    public function removeProduct($id)
    {
        $this->sale->removeItem($id);
    }

    public function showTotal()
    {
        return $this->sale->subTotal;
    }

    public function makePayment($amount, $method)
    {
        $invoice = $this->sale->makeInvoice($amount);
        $this->sendPayment($invoice);
    }

    public function sendPayment($invoice, \Payment $payment)
    {

    }

    public function findInformationProduct($id){

        Product::findInformation();
    }
}