<?php

/**
 * Created by PhpStorm.
 * User: anvargear
 * Date: 3/09/18
 * Time: 10:55 AM
 */
class Product
{

    public $name;
    public $sku;
    public $sock;
    public $attributes;

    function __construct()
    {

        $this->attributes = new ProductDescription();
    }

    public static function findInformation()
    {
        return new Product();
    }

    public function getDescription()
    {
        return ProductDescription::make();
    }
}